# RVGL Installer

This python script installs RVGL with game files and additional community packages.  
It can also update packages and the game.

The default RVGL directory is `~/.rvgl`.  
Additional data will be kept in these folders:  
- `.versions` for version numbers
- `.packs` for game data downloads, used for updates (if not kept they will be redownloaded)

## Usage

Run the script like this:
```
python3 install_rvgl.py
# or 
python install_rvgl.py
# or after setting executable permissions:
./install_rvgl.py
```

The installation wizard will guide you through the installation.  
All data will be downloaded from the re-volt.io server.

During the installation, you can press enter on questions to select the default option.  
For example:

`This will install RVGL. Continue? [Y/n]:`  
The default option is yes (Y). Press Enter to continue.

`Where would you like to install RVGL? [~/.rvgl]:`  
The default option is `~/.rvgl`. Press Enter to install the game there.

```
=== RVGL Installer ===
Usage:
install_rvgl                   Installs RVGL or updates when in game dir
install_rvgl install           Installs RVGL
install_rvgl update            Updates packages and RVGL
install_rvgl add *<packages>   Downloads and installs packages
install_rvgl packages          Displays available packages
install_rvgl check             Checks if an update for the installer is 
                                 available
```
