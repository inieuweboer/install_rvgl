# Changelog for the RVGL Installer

### 18.0428

#### Modifications
- Updates RVGL and assets if launched from the game folder without a command
- Stop asking for the shader version (since it has been merged with the alpha. Enable it in rvgl.ini)

#### Fixes
- Handle failed downloads better
---

### 18.0421

#### Fixes
- Use RVGL version number from distribute.re-volt.io

### 18.0419

#### Modifications
- The shader edition is now a standalone patch.
- Get package list from the website

#### Fixes
- Properly decode version strings

---

### 18.0414

#### Additions
- 'check' command to check if an update for the installer is available.

#### Modifications
- Changed the RVGL URL from revoltzone.net to re-volt.io.

#### Fixes
- Check for required directories when updating
