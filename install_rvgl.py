#!/usr/bin/python3

import os
import sys
import webbrowser
import urllib.request

user_path = os.path.expanduser("~")

installer_ver = 18.0428

package_url = "https://distribute.re-volt.io/packs/"
release_url = "https://distribute.re-volt.io/releases/"
rvgl_url = "https://rvgl.re-volt.io/downloads/"
help_url = "https://yethiel.gitlab.io/RVDocs/#gnulinux"
packagelist_url = "http://distribute.re-volt.io/packages.txt"
# rvgl_site = "http://rv12.revoltzone.net/rvgl.php"
rvgl_site = "https://rvgl.re-volt.io"
rvgl_version_url = "http://distribute.re-volt.io/releases/rvgl_version.txt"
installer_url = "https://gitlab.com/yethiel/install_rvgl/raw/master/install_rvgl.py"


def get_current_ver():
    """ Gets the current version of RVGL from the website """
    response = urllib.request.urlopen(rvgl_version_url)
    data = response.read()
    return data.decode('utf-8').strip()


def get_current_ver_installer():
    """ Gets the current version number of the installer """
    response = urllib.request.urlopen(installer_url)
    data = response.read()
    return data.decode('utf-8').split("installer_ver = ")[1][:7]


def get_packages():
    response = urllib.request.urlopen(packagelist_url)
    data = response.read()
    return data.decode('utf-8').split("\n")


def ask_bool(question, default=True):
    """ Asks for user input, ENTER can be used to confirm the 
        default option (written in caps) """
    if default is True:
        y = "Y"
        n = "n"
        ans = "y"
    else:
        y = "y"
        n = "N"
        ans = "n"
    
    response = input("{} [{}/{}]: ".format(question, y, n))

    if response.lower() == ans or response == "":
        return default
    else:
        return not default


def ask_string(question, default=""):
    """ Asks for user input, ENTER can be used to confirm the 
        default option """
    response = input("{} [{}]:".format(question, default))

    if response == "":
        return default
    else:
        return response


def ask_path(question, default="~/.rvgl"):
    """ Asks for a path and replaces ~ with the user path """
    response = ask_string(question, default)
    return response.replace("~", user_path)


def download(url, redl=False):
    """ Downloads a file if it doesn't exist already. The file can be force-
        downloaded by passing redl=True. """
    filename = url.split("/")[-1]
    if redl and os.path.isfile(filename):
        os.remove(filename)
    if not os.path.isfile(filename):
        os.system("wget -q --show-progress {}".format(url))
    else:
        print("File already exists. ({})".format(filename))

    if os.path.isfile(url.split("/")[-1]):
        return True
    else:
        print("Could not find {}".format(url.split("/")[-1]))
        return False


def download_pack(pack, redl=False):
    """ Downloads a pack. redl=True has to be passed for updates """
    print("Downloading {}...".format(pack))
    os.chdir(".packs")
    if not download("{}{}.zip".format(package_url, pack), redl=redl):
        print("Download failed.")
        exit()
    os.chdir("..")

    # Downloads the version file for later updates
    os.chdir(".versions")
    if not download("{}{}.txt".format(release_url, pack), redl=redl):
        print("Download failed.")
        exit()
    os.chdir("..")


def extract_pack(pack):
    """ Extracts a pack from the packs folder """
    if os.path.isfile(".packs/{}.zip".format(pack)):
        print("Extracting {}...".format(pack))
        os.system("unzip -o .packs/{}.zip > /dev/null".format(pack))


def download_rvgl(rvgl_ver):
    """ Downloads RVGL """
    print("Downloading RVGL...")
    os.chdir(".packs")
    print("{}rvgl_{}a_linux.7z".format(rvgl_url, rvgl_ver))
    
    if not download("{}rvgl_{}a_linux.7z".format(rvgl_url, rvgl_ver)):
        print("Download failed.")
        exit()
    os.chdir("..")


def extract_rvgl(rvgl_ver, ):
    """ Extracts RVGL """
    print("Extracting RVGL...")
    
    os.system("7z x .packs/rvgl_{}a_linux.7z -y > /dev/null".format(rvgl_ver))

    write_rvgl_version(rvgl_ver)


def get_dist_version(package):
    """ Gets the pack version from the website """
    try:
        url = "{}{}.txt".format(release_url, package)
        response = urllib.request.urlopen(url)
        data = response.read()
        return data.decode("utf-8").strip()
    except Exception as e:
        return 0.0


def get_local_version(package):
    """ Gets the pack version from the local .versions folder """
    fname = ".versions/{}.txt".format(package)
    if os.path.isfile(fname):
        with open(fname, "r") as f:
            return f.readline().strip()
    else:
        return 0.0


def write_rvgl_version(rvgl_ver):
    """ Writes the RVGL version to the .versions folder """
    with open(".versions/rvgl_version.txt", "w") as f:
        f.write(str(rvgl_ver))


def check_install_path(path):
    """ Checks if the installation path has all required folders """
    versions_dir = os.path.join(path, ".versions")
    if not os.path.isdir(versions_dir):
        os.makedirs(versions_dir)

    packs_dir = os.path.join(path, ".packs")
    if not os.path.isdir(packs_dir):
        os.makedirs(packs_dir)


def install():
    """ Installs the complete game """
    print("=== RVGL INSTALLER {} ===\n".format(installer_ver))
    if not ask_bool("This will install RVGL. Continue?"):
        exit()

    packages = get_packages()

    try:
        rvgl_ver = get_current_ver()
    except Exception as e:
        print("Could not connect. Are you online?\n    {}".format(e))
        exit()

    print("\nCurrent RVGL version: {}\n".format(rvgl_ver))

    install_path = ask_path("Where would you like to install RVGL?")

    if not os.path.isdir(install_path):
        try:
            os.makedirs(install_path)
            print("Created \"{}\"".format(install_path))
        except Exception as e:
            print("Could not create directory:\n    {}".format(e))
            exit()

    print("")
    print("RVGL will be installed to \"{}\".".format(install_path))
    if len(os.listdir(install_path)) > 0:
        print("Warning: Directory is not empty!")
    print("")


    dl_soundtrack = ask_bool(
        "Would you like to download the soundtrack? (~100MB)"
    )
    print("")
    dl_io = ask_bool("Would you like to download the online packs?\n"
                "(additional tracks and cars) (~420MB)")
    print("")

    try:
        os.chdir(install_path)
    except Exception as e:
        print("Cannot change to install directory:\n    {}".format(e))
        exit()

    if not os.path.isdir(".versions"):
        os.mkdir(".versions")
    if not os.path.isdir(".packs"):
        os.mkdir(".packs")

    download_rvgl(rvgl_ver)
    
    download_pack("game_files")
    download_pack("rvgl_dcpack")
    
    if dl_soundtrack:
        download_pack("soundtrack")
    
    if dl_io:
        print("Downloading I/O online packs...")
        for package in packages:
            if not "io_" in package:
                continue
            download_pack(package)

    print("\nExtracting packs...")
    
    for pack in packages:
        extract_pack(pack)
    
    extract_rvgl(rvgl_ver)

    print("\nDone.\n")

    print("Starting RVGL setup...")
    os.system("./setup")

    if not ask_bool("Keep installation files? (.packs/)"):
        os.system("rm -r .packs")

    if ask_bool("\nSome libraries need to be installed.\nWould you like to open the help page?"):
        webbrowser.open(
            help_url
        )
    else:
        print("You can view information about required packages here:\n{}".format(help_url))

    print(
        "\nInstallation complete.\n"
        "To start RVGL, cd to \"{}\" and type ./rvgl or use the desktop file.".format(install_path)
    )


def update():
    """ Searches for pack and game updates """

    packages = get_packages()

    if not "rvgl" in os.listdir():
        install_path = ask_path(
            "Which installation would you like to update?"
        )
    else:
        print("Using the RVGL installation in the current folder.")
        install_path = "."
    
    check_install_path(install_path)
    os.chdir(install_path)

    print("")

    installed_packages = [
        p.replace(".txt", "") for p in os.listdir(".versions") if p.replace(".txt", "") in packages
    ]
    updates = []
    for package in installed_packages:
        local_ver = get_local_version(package)
        dist_ver = get_dist_version(package)
        if float(local_ver) < float(dist_ver):
            print("Update available for {}: {}".format(package, dist_ver))
            updates.append(package)
        else:
            print("Package {} is up to date: {}".format(package, local_ver))

    if updates:
        print("\nUpdates are available for the following packages:")
        for package in updates:
            print(package)

        if ask_bool("\nInstall package updates?"):
            print("")
            for package in updates:
                download_pack(package, redl=True)
                extract_pack(package)

    print("")

    rvgl_ver = get_current_ver()
    local_rvgl_ver = get_local_version("rvgl_version")

    if float(local_rvgl_ver) < float(rvgl_ver) or "game_files" in updates:
        print("Update available for RVGL: {}".format(rvgl_ver))
        download_rvgl(rvgl_ver)
        print("")
        extract_rvgl(rvgl_ver)
        print("")

    else:
        print("RVGL is up to date: {}\n".format(local_rvgl_ver))

    print("Updates complete.")


def print_usage():
    """ Prints the usage message """
    print(
        "=== RVGL Installer {} ===\n"
        "Usage:\n"
        "install_rvgl                   Installs RVGL or updates when in game dir\n"
        "install_rvgl install           Installs RVGL\n"
        "install_rvgl update            Updates packages and RVGL\n"
        "install_rvgl add *<packages>   Downloads and installs packages\n"
        "install_rvgl packages          Displays available packages\n"
        "install_rvgl check             Checks if an update for the installer is \n"
        "                                 available\n"
        "".format(installer_ver)
    )


if len(sys.argv) <= 1:
    if "rvgl" in os.listdir():
        update()
    else:
        install()

elif sys.argv[1] == "install":
    install()

elif sys.argv[1] == "update":
    update()

elif sys.argv[1] == "packages":
    print("Available packages:")
    for pack in get_packages():
        print(pack)

elif sys.argv[1] == "add":

    packages = get_packages()
    
    if len(sys.argv) == 2:
        print_usage()
        exit()

    for package in sys.argv[2:]:
        install_path = ask_string(
            "Where would you like to install the packages?", 
            default="~/.rvgl"
        )
        install_path = install_path.replace("~", user_path)
        os.chdir(install_path)
        if package in packages:
            download_pack(package)
            extract_pack(package)

        else:
            print("Unknown package: {}".format(package))

    print("\nDone.")

elif sys.argv[1] == "check":
    new_ver = get_current_ver_installer()
    print("Current version:", installer_ver)
    print("Available version:", new_ver)

    if float(new_ver) > float(installer_ver):
        print("Update available from\n{}".format(installer_url))
    elif float(new_ver) == float(installer_ver):
        print("No update available.")
    else:
        print("You're a time traveler.")

else:
    print_usage()